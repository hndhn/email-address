import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailAddress {
  private String address;
  public EmailAddress(String address) {
    this.address = address;
  }

  public boolean isValid() {
    String pattern = "[a-zA-Z](\\w+[^-#$%^&*()])@(\\w+[^-#$%^&*()])\\w+(.[a-zA-Z](\\w+[^-#$%^&*()]))*";
	Pattern r = Pattern.compile(pattern);
	Matcher m = r.matcher(address);
	if (m.find()) return true;
	else return false;
  }
}
